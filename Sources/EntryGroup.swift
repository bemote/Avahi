
import CAvahi.Client


protocol AvahiEntryGroupDelegate {
    func entryGroupRegistering()
    func entryGroupFailure()
    func entryGroupEstablished()
}
extension AvahiEntryGroupDelegate {
    func entryGroupRegistering(){}
    func entryGroupFailure(){}
    func entryGroupEstablished(){}
}

public class AvahiEntryGroup {
    var c_entry_group: OpaquePointer?
    var delegate: AvahiEntryGroupDelegate?

    public init(client: AvahiClient){

        let unsafeSelf = UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())
        
        let the_client = client.c_client

        c_entry_group = avahi_entry_group_new(the_client, {
            group, state, data in
            let this = Unmanaged<AvahiEntryGroup>.fromOpaque(data!).takeUnretainedValue()
            this.c_entry_group = group
            this.entryCallback(state)
        }, unsafeSelf)
    }

    func entryCallback(_ state: AvahiEntryGroupState){

        switch state {
        case AVAHI_ENTRY_GROUP_UNCOMMITED: 	
            print("uncommited")

        case AVAHI_ENTRY_GROUP_REGISTERING: 
            delegate?.entryGroupRegistering()
            print("registering")	

        case AVAHI_ENTRY_GROUP_ESTABLISHED: 
            delegate?.entryGroupEstablished()
            print("established")	

        case AVAHI_ENTRY_GROUP_COLLISION: 	
            print("collision")

        case AVAHI_ENTRY_GROUP_FAILURE: 
            delegate?.entryGroupFailure()
            print("failure")
        default:
            break;
        }
    }

    public func addService(_ name: String, type: String, port: UInt16)
    {
         let empty = avahi_entry_group_is_empty(c_entry_group)

         if empty != 0 {

            let interfaces = AvahiIfIndex(AVAHI_IF_UNSPEC)
            let protocols = AvahiProtocol(AVAHI_PROTO_UNSPEC)
            let flags = AvahiPublishFlags(0)

            avahi_entry_group_add_service_strlst(c_entry_group, 
                interfaces, 
                protocols, 
                flags, name, type, nil, nil, port, nil)
            
            avahi_entry_group_commit(c_entry_group)

        }
    }
}
