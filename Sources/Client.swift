
import CAvahi.Client

protocol AvahiClientDelegate {
    func clientRunning()
    func clientFailure()
    func clientConnecting()
}
extension AvahiClientDelegate {
    func clientRunning(){}
    func clientFailure(){}
    func clientConnecting(){}
}

public class AvahiClient {

    var c_client: OpaquePointer?
    var mPoll:AvahiSimplePoll

    var delegate: AvahiClientDelegate?

    let managedPoll: Bool

    public init(poll: AvahiSimplePoll? = nil){
        if poll == nil {
            mPoll = AvahiSimplePoll()
            managedPoll = true
        }
        else {
            mPoll = poll!
            managedPoll = false
        }

        
        let unsafeSelf = UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())

        c_client = avahi_client_new(mPoll.getPoll(), AvahiClientFlags(0), {
            client, state, data in

            let this = Unmanaged<AvahiClient>.fromOpaque(data!).takeUnretainedValue()

            this.c_client = client

            this.clientCallback(state)

        }, unsafeSelf, nil)

        if managedPoll {
            mPoll.asyncLoop()
        }
    }

    func clientCallback(_ state: AvahiClientState){

        switch state {
        case AVAHI_CLIENT_S_REGISTERING:
            print("client registering")
        case AVAHI_CLIENT_S_RUNNING:
            delegate?.clientRunning()
            print("client running")
        case AVAHI_CLIENT_S_COLLISION:
            print("client collision")
        case AVAHI_CLIENT_FAILURE:
            delegate?.clientFailure()
            print("client failure")
        case AVAHI_CLIENT_CONNECTING:
            delegate?.clientConnecting()
            print("client connecting")
        default:
            break
        }
    }
}

