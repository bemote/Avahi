
import CAvahi.Client
import Dispatch
import Foundation

public class AvahiSimplePoll {

    let c_simple_poll:OpaquePointer

    let pollQueue: DispatchQueue

    public init(){
        c_simple_poll = avahi_simple_poll_new()
        pollQueue = DispatchQueue(label: "poll_queue", target: DispatchQueue.global())
    }

    func getPoll()->UnsafePointer<AvahiPoll>!{
        return avahi_simple_poll_get(c_simple_poll)
    }

    func asyncLoop() {
        pollQueue.async {
            self.loop()   
        }
    }

    func loop() {
        avahi_simple_poll_loop(c_simple_poll)
    }

    func quit() {
        avahi_simple_poll_quit(c_simple_poll)
    }

}