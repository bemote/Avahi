import PackageDescription

let package = Package(
    name: "Avahi",
    dependencies:[
        .Package(url: "https://gitlab.com/bemote/CAvahi.git",
                 majorVersion: 1),
    ]
)
